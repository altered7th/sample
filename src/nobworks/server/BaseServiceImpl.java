package nobworks.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import nobworks.client.GreetService;

@SuppressWarnings("serial")
public class BaseServiceImpl extends RemoteServiceServlet implements GreetService {

	@Override
	public String serviceProc( String input ) throws IllegalArgumentException {
		String serverInfo = getServletContext().getServerInfo();
		String userAgent = getThreadLocalRequest().getHeader( "User-Agent" );

		// Escape data from the client to avoid cross-site script vulnerabilities.
		input = escapeHtml( input );
		userAgent = escapeHtml( userAgent );

		return input + "!<br><br>I am running " + serverInfo + ".<br><br>It looks like you are using:<br>"
				+ userAgent;
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	protected String escapeHtml( String html ) {
		if ( html == null ) {
			return null;
		}
		return html.replaceAll( "&", "&amp;" ).replaceAll( "<", "&lt;" ).replaceAll( ">", "&gt;" );
	}
}
