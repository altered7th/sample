package nobworks.server;

import nobworks.client.GreetService;
import nobworks.shared.FieldVerifier;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends BaseServiceImpl implements GreetService {

	@Override
	public String serviceProc( String input ) throws IllegalArgumentException {
		// Verify that the input is valid. 
		if ( !FieldVerifier.isValidName( input ) ) {
			// If the input is not valid, throw an IllegalArgumentException back to
			// the client.
			throw new IllegalArgumentException( "Name must be at least 4 characters long" );
		}
		return "Hello," + super.serviceProc( input );
	}
}