package nobworks.client;

import com.google.gwt.core.client.GWT;

public class AddressUI extends BaseUI {

	public AddressUI() {
		super();
		textField.setWidth( "200px" );
	}

	@Override
	protected void setWidgetProperties() {
		buttonTitle = "送信する";
		textValue = "";
		buttonStyleName = null;
		buttonTagName = "sendButtonContainer2";
		textTagName = "nameFieldContainer2";
		serviceAsync = GWT.create( AddressService.class );
	}
	@Override
	protected String validateInput() {
		return null;
	}

}
