package nobworks.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;

import nobworks.shared.FieldVerifier;

public abstract class BaseUI {

	private Button sendButton;
	protected TextBox textField;
	private static Label errorLabel;
	
	protected String buttonTitle;
	protected String textValue;
	protected String buttonStyleName;
	protected String buttonTagName;
	protected String textTagName;
	protected BaseServiceAsync serviceAsync;
	
	public BaseUI() {
		setWidgetProperties();
		createUI();
		new MyHandler( this );
	}
	protected abstract void setWidgetProperties();
	protected abstract String validateInput();
	private void createUI() {
		if ( errorLabel == null ) {
			errorLabel = new Label();
			RootPanel.get( "errorLabelContainer" ).add( errorLabel );
		}
		sendButton = new Button( buttonTitle );
		if ( buttonStyleName != null ) {
			sendButton.addStyleName( buttonStyleName );
		}
		textField = new TextBox();
		textField.setText( textValue );
		RootPanel.get( textTagName ).add( textField );
		RootPanel.get( buttonTagName ).add( sendButton );
	}
	public String getTextToServer() {
		// First, we validate the input.
		errorLabel.setText( "" );
		String textToServer = textField.getText();
		String errorMessage = validateInput();
		if ( errorMessage != null ) {
			errorLabel.setText( errorMessage );
			return null;
		}
		// Then, we send the input to the server.
		sendButton.setEnabled( false );
		return textToServer;
	}
	public void enableSendButton() {
		sendButton.setEnabled( true );
		sendButton.setFocus( true );	
	}
	public void setHandler( MyHandler handler ) {
		sendButton.addClickHandler( handler );
		textField.addKeyUpHandler( handler );
	}
	public BaseServiceAsync getServiceAsync() {
		return serviceAsync;
	}
}
