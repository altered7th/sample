package nobworks.client;

import com.google.gwt.user.client.rpc.RemoteService;

public interface BaseService extends RemoteService {
	String serviceProc( String input ) throws IllegalArgumentException;
}
