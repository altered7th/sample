package nobworks.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;

import nobworks.shared.FieldVerifier;

public class NameUI extends BaseUI {

	public NameUI() {
		super();
		
		textField.setFocus( true );
		textField.selectAll();
	}
	@Override
	protected void setWidgetProperties() {
		buttonTitle = "Send";
		textValue = "GWT User";
		buttonStyleName = "sendButton";
		buttonTagName = "sendButtonContainer";
		textTagName = "nameFieldContainer";
		serviceAsync = GWT.create( GreetService.class );
	}
	@Override
	protected String validateInput() {
		String textToServer = textField.getText();
		if ( !FieldVerifier.isValidName( textToServer ) ) {
			return "Please enter at least four characters";
		} else {
			return null;
		}
	}
	
}
