package nobworks.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

import nobworks.shared.FieldVerifier;

public class MyHandler implements ClickHandler, KeyUpHandler {

	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network " + "connection and try again.";

	private BaseUI baseUI;
	private BaseDialogBox dialogBox;

	public MyHandler( BaseUI baseUI ) {
		this.baseUI = baseUI;
		dialogBox = new BaseDialogBox( baseUI );
		baseUI.setHandler( this );
	}
	public void onClick( ClickEvent event ) {
		sendTextToServer();
	}

	public void onKeyUp( KeyUpEvent event ) {
		if ( event.getNativeKeyCode() == KeyCodes.KEY_ENTER ) {
			sendTextToServer();
		}
	}
	private void sendTextToServer() {
		String textToServer = baseUI.getTextToServer();
		if ( textToServer == null ) {
			return;
		}
		
		dialogBox.resetLabelContents( textToServer );
		
		baseUI.getServiceAsync().serviceProc( textToServer, new AsyncCallback<String>() {
			public void onFailure( Throwable caught ) {
				dialogBox.setServiceResult( false, "Remote Procedure Call - Failure", SERVER_ERROR );
			}
			public void onSuccess( String result ) {
				dialogBox.setServiceResult( true, "Remote Procedure Call", result );
			}
		} );
	}
}
