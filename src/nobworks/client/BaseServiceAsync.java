package nobworks.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface BaseServiceAsync {
	void serviceProc( String input,
		AsyncCallback<String> callback ) throws IllegalArgumentException;
}
