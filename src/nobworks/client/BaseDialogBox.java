package nobworks.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class BaseDialogBox extends DialogBox {
	
	final private static String ERROR_STYLE_NAME = "serverResponseLabelError";
	private Label textToServerLabel;
	private HTML serverResponseLabel;
	private Button closeButton;
	private BaseUI baseUI;

	public BaseDialogBox( BaseUI baseUI ) {
		this.baseUI = baseUI;
		createUI();
		createHandler();
	}
	private void createHandler() {
		closeButton.addClickHandler( new ClickHandler() {
			public void onClick( ClickEvent event ) {
				hide();
				baseUI.enableSendButton();
			}
		} );	
	}
	private void createUI() {
		setText( "Remote Procedure Call" );
		setAnimationEnabled( true );
		closeButton = new Button( "Close" );
		// We can set the id of a widget by accessing its Element
		closeButton.getElement().setId( "closeButton" );
		textToServerLabel = new Label();
		serverResponseLabel = new HTML();
		VerticalPanel dialogVPanel = new VerticalPanel();
		dialogVPanel.addStyleName( "dialogVPanel" );
		dialogVPanel.add( new HTML( "<b>Sending name to the server:</b>" ) );
		dialogVPanel.add( textToServerLabel );
		dialogVPanel.add( new HTML( "<br><b>Server replies:</b>" ) );
		dialogVPanel.add( serverResponseLabel );
		dialogVPanel.setHorizontalAlignment( VerticalPanel.ALIGN_RIGHT );
		dialogVPanel.add( closeButton );
		setWidget( dialogVPanel );
	}
	public void setServiceResult( boolean success, String title, String result ) {
		setText( title );
		if ( success ) {
			serverResponseLabel.removeStyleName( ERROR_STYLE_NAME );
		} else {
			serverResponseLabel.addStyleName( ERROR_STYLE_NAME );
		}
		serverResponseLabel.setHTML( result );
		center();
		closeButton.setFocus( true );
	}
	public void resetLabelContents( String textToServer ) {
		textToServerLabel.setText( textToServer );
		serverResponseLabel.setText( "" );
	}
}
