package nobworks.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Gwt1 implements EntryPoint {
	public void onModuleLoad() {
		new NameUI();
		new AddressUI();
	}
}
